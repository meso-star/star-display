# Star Display

This program displays an image of a geometry or a list of geometries.

## How to build

The library relies on the [CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/) package to build. It also depends
on the [RSys](https://gitlab.com/vaplv/rsys/),
[Star-3D](https://gitlab.com/meso-star/star-3d/),
[Star-3DAW](https://gitlab.com/meso-star/star-3daw/),
[Star-3DSTL](https://gitlab.com/meso-star/star-3dstl/) and
[Star-Renderer](https://gitlab.com/meso-star/star-rdr/) libraries.
Note that if the [SDL](https://www.libsdl.org/) 1.2 library is available, one
can enabled the support of interactive rendering by enabling the
`INTERACTIVE` CMake variable.

First ensure that CMake is installed on your system. Then install the RCMake
package as well as all the aforementioned prerequisites. Then generate the
project from the `cmake/CMakeLists.txt` file by appending to the
`CMAKE_PREFIX_PATH` variable the install directories of its dependencies.

## License

Star Display is Copyright (C) |Meso|Star> 2015-2017 (<contact@meso-star.com>).
It is a free software released under the [OSI](http://opensource.org)-approved
CeCILL license. You are welcome to redistribute it under certain conditions;
refer to the COPYING files for details.

