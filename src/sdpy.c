/* Copyright (C) |Meso|Star> 2015-2017 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include "sdpy_args.h"
#include "sdpy_view.h"

#include <rsys/float3.h>
#include <rsys/image.h>
#include <rsys/mem_allocator.h>

#include <star/s3d.h>
#include <star/s3daw.h>
#include <star/s3dstl.h>
#include <star/srdr.h>

#ifdef SDPY_USE_SDL
  #include "sdpy_input.h"
  #include <SDL.h>
#endif

#include <string.h>

struct sdpy {
  struct s3d_device* s3d;
  struct s3d_scene* scn;
  struct srdr_device* srdr;
  struct srdr_framebuffer* fbuf;
  struct srdr_render_buffer* rbuf;
  struct sdpy_view* view;
#ifdef SDPY_USE_SDL
  struct sdpy_input* input;
  SDL_Surface* surface;
#endif
};

static const struct sdpy SDPY_NULL = {
  NULL, NULL, NULL, NULL, NULL, NULL
#ifdef SDPY_USE_SDL
  , NULL, NULL
#endif
};

/*******************************************************************************
 * Helper function
 ******************************************************************************/
#ifdef SDPY_USE_SDL
static res_T
init_interactive_window(struct sdpy* sdpy, const size_t vmode[2])
{
  const uint32_t vmask = SDL_HWSURFACE|SDL_ANYFORMAT;
  int bpp = 32;
  res_T res = RES_OK;

  if(SDL_Init(SDL_INIT_VIDEO)) {
    fprintf(stderr, "Couldn't initialise SDL: %s\n", SDL_GetError());
    res = RES_UNKNOWN_ERR;
    goto error;
  }
  if(SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL)) {
    fprintf(stderr, "Couldn't enable key repeat.\n");
    res = RES_UNKNOWN_ERR;
    goto error;
  }

  SDL_WM_SetCaption("Star-Display", NULL);
  bpp = SDL_VideoModeOK((int)vmode[0], (int)vmode[1], bpp, vmask);
  if(!bpp) {
    fprintf(stderr, "Unavailable video mode %lux%lu@32bpp.\n",
      (unsigned long)vmode[0], (unsigned long)vmode[1]);
    res = RES_BAD_ARG;
    goto error;
  }

  sdpy->surface = SDL_SetVideoMode((int)vmode[0], (int)vmode[1], bpp, vmask);
  if(!sdpy->surface) {
    fprintf(stderr,
      "Couldn't set the video mode %lux%lu@32bpp.\n",
      (unsigned long)vmode[0], (unsigned long)vmode[1]);
    res = RES_BAD_ARG;
    goto error;
  }

  res = sdpy_input_create
    (SDPY_INPUT_DEFAULT_KEY_BINDING,
     SDPY_INPUT_DEFAULT_BUTTON_BINDING,
     &sdpy->input);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  SDL_Quit();
  sdpy->surface = NULL;
  goto exit;
}

/* Write into a SDL_surface */
static res_T
surface_write
  (void* ctx,
   const size_t origin[2],
   const size_t size[2],
   const enum srdr_pixel_format format,
   const void* pixels)
{
  struct SDL_Surface* surface = ctx;
  struct SDL_PixelFormat* pixfmt;
  size_t Bpp_src; /* Bytes per pixel */
  size_t pitch_src;
  size_t ix, iy;
  ASSERT(ctx && origin && size);

  if(format != SRDR_UBYTE_RGBA) {
    fprintf(stderr, "Only UBYTE_RGBA framebuffer are currently supported\n");
    return RES_BAD_ARG;
  }

  Bpp_src = SRDR_SIZEOF_PIXEL_FORMAT(format);
  pitch_src = Bpp_src * size[0];
  pixfmt = surface->format;

  FOR_EACH(iy, 0, size[1]) {
    char* row_dst = (char*)surface->pixels + (iy + origin[1]) * surface->pitch;
    char* row_src = (char*)pixels + iy * pitch_src;

    FOR_EACH(ix, 0, size[0]) {
      char* pixdst = row_dst + (ix + origin[0]) * pixfmt->BytesPerPixel;
      uint8_t* pixsrc = (uint8_t*)(row_src + ix * Bpp_src);
      uint32_t rgba;
      rgba = SDL_MapRGBA(pixfmt, pixsrc[0], pixsrc[1], pixsrc[2], pixsrc[3]);

      switch(pixfmt->BytesPerPixel) {
        case 1: *(uint8_t*)pixdst = (uint8_t)rgba; break;
        case 2: *(uint16_t*)pixdst = (uint16_t)rgba; break;
        case 3:
#ifdef COMPILER_CL
  #pragma warning(push)
  #pragma warning(disable:4127) /* Constant conditional expression */
          if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
  #pragma warning(pop)
#else
          if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
#endif
            ((uint8_t*)pixdst)[0] = (rgba >> 16) & 0xFF;
            ((uint8_t*)pixdst)[1] = (rgba >> 8)  & 0xFF;
            ((uint8_t*)pixdst)[2] = (rgba >> 0)  & 0xff;
          } else {
            ((uint8_t*)pixdst)[2] = (rgba >> 16) & 0xFF;
            ((uint8_t*)pixdst)[1] = (rgba >> 8)  & 0xFF;
            ((uint8_t*)pixdst)[0] = (rgba >> 0)  & 0xff;
          }
          break;
        case 4: *(uint32_t*)pixdst = rgba; break;
        default: FATAL("Unreachable code\n"); break;
      }
    }
  }
  return RES_OK;
}

static INLINE void
view_update
  (struct sdpy_view* view,
   const float rot_speed,
   const float trans_speed,
   const struct sdpy_events* e)
{
  /* Emprical factor that scales the key events speed factors. This is required
   * since the mouse movements are based on the differential mouse movements
   * that are far more greater than the key event ticks */
  const float KEY_SPEED_SCALE = 30.f;
  float vec[3];
  ASSERT(view && e);

  /* Translation */
  vec[0] = (float)(e->keys[SDPY_KEY_MOVE_RIGHT] - e->keys[SDPY_KEY_MOVE_LEFT]);
  vec[1] = (float)(e->keys[SDPY_KEY_MOVE_UP] - e->keys[SDPY_KEY_MOVE_DOWN]);
  vec[2] = (float)(e->keys[SDPY_KEY_MOVE_FORWARD] - e->keys[SDPY_KEY_MOVE_BACKWARD]);
  f3_mulf(vec, vec, trans_speed * KEY_SPEED_SCALE);
  if(e->buttons & SDPY_BUTTON_FLAG(SDPY_BUTTON_MOTION_MOVE)) {
    vec[0] -= (float)e->mouse_dx * trans_speed;
    vec[1] += (float)e->mouse_dy * trans_speed;
  }
  sdpy_view_translate(view, vec);

  /* Rotation */
  vec[0] = (float)(e->keys[SDPY_KEY_LOOK_UP] - e->keys[SDPY_KEY_LOOK_DOWN]);
  vec[1] = (float)(e->keys[SDPY_KEY_LOOK_RIGHT] - e->keys[SDPY_KEY_LOOK_LEFT]);
  vec[2] = 0.f;
  f3_mulf(vec, vec, rot_speed * KEY_SPEED_SCALE);
  if(e->buttons & SDPY_BUTTON_FLAG(SDPY_BUTTON_MOTION_LOOK)) {
    vec[0] -= (float)e->mouse_dy * rot_speed;
    vec[1] += (float)e->mouse_dx * rot_speed;
  }
  sdpy_view_rotate(view, vec);
}
#endif

static res_T
register_model_obj
  (struct sdpy* sdpy,
   struct s3daw* s3daw,
   const char* model_name)
{
  size_t ishape, nshapes;
  res_T res = RES_OK;
  ASSERT(sdpy && s3daw && model_name);

  res = s3daw_get_shapes_count(s3daw, &nshapes);
  if(res != RES_OK) {
    fprintf(stderr,
      "Couldn't retrieve the number of Star3D shapes created from '%s'.\n",
      model_name);
    goto error;
  }

  res = s3d_scene_clear(sdpy->scn);
  if(res != RES_OK) {
    fprintf(stderr, "Couldn't clean-up the Star3D scene.\n");
    goto error;
  }

  FOR_EACH(ishape, 0, nshapes) {
    struct s3d_shape* shape;
    res = s3daw_get_shape(s3daw, ishape, &shape);
    if(res != RES_OK) {
      fprintf(stderr,
        "Couldn't retrieve the Star3D shape '%lu' of '%s'.\n",
        (unsigned long)ishape, model_name);
      goto error;
    }
    res = s3d_scene_attach_shape(sdpy->scn, shape);
    if(res != RES_OK) {
      fprintf(stderr,
        "Couldn't attach the Star3D shape '%lu' of '%s' to the Star3D scene.\n",
        (unsigned long)ishape, model_name);
      goto error;
    }
  }
exit:
  return res;
error:
  S3D(scene_clear(sdpy->scn));
  goto exit;
}

static res_T
register_model_stl
  (struct sdpy* sdpy,
   struct s3dstl* s3dstl,
   const char* model_name)
{
  struct s3d_shape* shape;
  res_T res = RES_OK;
  ASSERT(sdpy && s3dstl && model_name);

  res = s3d_scene_clear(sdpy->scn);
  if(res != RES_OK) {
    fprintf(stderr, "Couldn't clean-up the Star3D scene.\n");
    goto error;
  }

  res = s3dstl_get_shape(s3dstl, &shape);
  if(res != RES_OK) {
    fprintf(stderr,
      "Couldn't retrieve the Star3D shape of '%s'.\n", model_name);
    goto error;
  }
  if(shape) {
    res = s3d_scene_attach_shape(sdpy->scn, shape);
    if(res != RES_OK) {
      fprintf(stderr,
        "Couldn't attach the Star3D shape of '%s' to the Star3D scene.\n",
        model_name);
      goto error;
    }
  }
exit:
  return res;
error:
  S3D(scene_clear(sdpy->scn));
  goto exit;
}

static res_T
auto_look_at
  (struct s3d_scene_view* view,
   const float fov_x, /* Horizontal field of view in radian */
   const float proj_ratio, /* Width / height */
   const float up[3], /* Up vector */
   float position[3],
   float target[3])
{
  float lower[3], upper[3];
  float up_abs[3];
  float axis_min[3];
  float axis_x[3];
  float axis_z[3];
  float tmp[3];
  float radius;
  float depth;
  int i;
  res_T res;
  ASSERT(up && position && target);

  res = s3d_scene_view_get_aabb(view, lower, upper);
  if(res != RES_OK) {
    fprintf(stderr, "Couldn't retrieve the scene bounding box.\n");
    goto error;
  }
  /* Assume not empty scene */
  ASSERT(lower[0] < upper[0] && lower[1] < upper[1] && lower[2] < upper[2]);

  /* The target is the scene centroid */
  f3_mulf(target, f3_add(target, lower, upper), 0.5f);

  /* Define which up dimension is minimal and use its unit vector to compute a
   * vector orthogonal to `up'. This ensures that the unit vector and `up' are
   * not collinear and thus that their cross product is not a zero vector. */
  FOR_EACH(i, 0, 3) up_abs[i] = absf(up[i]);
  if(up_abs[0] < up_abs[1]) {
    if(up_abs[0] < up_abs[2]) f3(axis_min, 1.f, 0.f, 0.f);
    else f3(axis_min, 0.f, 0.f, 1.f);
  } else {
    if(up_abs[1] < up_abs[2]) f3(axis_min, 0.f, 1.f, 0.f);
    else f3(axis_min, 0.f, 0.f, 1.f);
  }
  f3_normalize(axis_x, f3_cross(axis_x, up, axis_min));
  f3_normalize(axis_z, f3_cross(axis_z, up, axis_x));

  /* Approximate whether on the XYZ or the ZYX basis the visible part of the
   * model is maximise */
  if(absf(f3_dot(axis_x, upper)) < absf(f3_dot(axis_z, upper))) {
    SWAP(float, axis_x[0], axis_z[0]);
    SWAP(float, axis_x[1], axis_z[1]);
    SWAP(float, axis_x[2], axis_z[2]);
  }

  /* Ensure that the whole model is visible */
  radius = f3_len(f3_sub(tmp, upper, lower)) * 0.5f;
  if(proj_ratio < 1.f) {
    depth = radius / (float)sin(fov_x/2.0);
  } else {
    depth = radius / (float)sin(fov_x/(2.f*proj_ratio));
  }
  /* Define the camera position */
  f3_sub(position, target, f3_mulf(tmp, axis_z, depth));

  /* Empirically move the position to find a better point of view */
  f3_add(position, position, f3_mulf(tmp, up, radius)); /*Empirical offset*/
  f3_add(position, position, f3_mulf(tmp, axis_x, radius)); /*Empirical offset*/
  f3_normalize(tmp, f3_sub(tmp, target, position));
  f3_sub(position, target, f3_mulf(tmp, tmp, depth));

exit:
  return res;
error:
  goto exit;
}

static void
define_move_sensibility(struct s3d_scene_view* view, float* translation)
{
  float lower[3], upper[3];
  float f;
  ASSERT(view && translation);
  S3D(scene_view_get_aabb(view, lower, upper));
  f = MMIN(MMIN(upper[0]-lower[0], upper[1]-lower[1]), upper[2]-lower[2]);
  *translation = f * 0.002f/* Emperical offset */;
}

/*******************************************************************************
 * SDPY functions
 ******************************************************************************/
static void
sdpy_release(struct sdpy* sdpy)
{
  ASSERT(sdpy);
  if(sdpy->s3d) S3D(device_ref_put(sdpy->s3d));
  if(sdpy->scn) S3D(scene_ref_put(sdpy->scn));
  if(sdpy->srdr) SRDR(device_ref_put(sdpy->srdr));
  if(sdpy->rbuf) SRDR(render_buffer_ref_put(sdpy->rbuf));
  if(sdpy->fbuf) SRDR(framebuffer_ref_put(sdpy->fbuf));
  if(sdpy->view) sdpy_view_ref_put(sdpy->view);
#ifdef SDPY_USE_SDL
  if(sdpy->surface) SDL_Quit();
  if(sdpy->input) sdpy_input_ref_put(sdpy->input);
#endif
}

static res_T
sdpy_init(struct sdpy* sdpy, const struct sdpy_args* args)
{
  res_T res = RES_OK;
  ASSERT(sdpy && args);
  *sdpy = SDPY_NULL;

  res = s3d_device_create(NULL, NULL, 0, &sdpy->s3d);
  if(res != RES_OK) {
    fprintf(stderr, "Couldn't create the Star3D device.\n");
    goto error;
  }
  res = s3d_scene_create(sdpy->s3d, &sdpy->scn);
  if(res != RES_OK) {
    fprintf(stderr, "Couldn't create the Star3D scn.\n");
    goto error;
  }
  res = srdr_device_create(NULL, sdpy->s3d, SRDR_NTHREADS_DEFAULT, &sdpy->srdr);
  if(res != RES_OK) {
    fprintf(stderr, "Couldn't create the Star Renderer.\n");
    goto error;
  }
  res = sdpy_view_create(sdpy->srdr, &sdpy->view);
  if(res != RES_OK) goto error;

  res = srdr_framebuffer_create(sdpy->srdr, &sdpy->fbuf);
  if(res != RES_OK) {
    fprintf(stderr, "Couldn't create the Star Framebuffer.\n");
    goto error;
  }

  res = srdr_render_buffer_create(sdpy->srdr, &sdpy->rbuf);
  if(res != RES_OK) {
    fprintf(stderr, "Couldn't create the Star Render Buffer.\n");
    goto error;
  }
  res = srdr_render_buffer_setup(sdpy->rbuf, args->vmode, SRDR_UBYTE_RGBA);
  if(res != RES_OK) {
    fprintf(stderr, "Couldn't setup the Star Render Buffer to %lux%lu.\n",
      (unsigned long)args->vmode[0], (unsigned long)args->vmode[1]);
    goto error;
  }

#ifdef SDPY_USE_SDL
  if(args->interactive) {
    /* Draw directly into the SDL surface */
    res = init_interactive_window(sdpy, args->vmode);
    if(res != RES_OK) goto error;
    res = srdr_framebuffer_setup
      (sdpy->fbuf, args->vmode, surface_write, sdpy->surface);
    if(res != RES_OK) {
      fprintf(stderr, "Couldn't setup the Star Framebuffer to %lux%lu.\n",
        (unsigned long)args->vmode[0], (unsigned long)args->vmode[1]);
    }
  } else
#endif
  {
    /* Draw into an offline render buffer */
    res = srdr_framebuffer_setup
      (sdpy->fbuf, args->vmode, srdr_render_buffer_write, sdpy->rbuf);
    if(res != RES_OK) {
      fprintf(stderr, "Couldn't setup the Star Framebuffer to %lux%lu.\n",
        (unsigned long)args->vmode[0], (unsigned long)args->vmode[1]);
    }
  }
exit:
  return res;
error:
  sdpy_release(sdpy);
  *sdpy = SDPY_NULL;
  goto exit;
}

static res_T
dump_render_buffer(struct srdr_render_buffer* rbuf, FILE* dst, const char* name)
{
  struct srdr_image img = SRDR_IMAGE_NULL;
  size_t x, y;
  res_T res = RES_OK;
  ASSERT(rbuf && dst);

  res = srdr_render_buffer_image_get(rbuf, &img);
  if(res != RES_OK) {
    fprintf(stderr, "Couldn't get the render pixel buffer.\n");
    return res;
  }

  fprintf(dst, "P3\n%lu %lu\n255\n",
    (unsigned long)img.size[0], (unsigned long)img.size[1]);
  FOR_EACH(y, 0, img.size[1]) {
    const char* row = img.data + y*img.pitch;
    FOR_EACH(x, 0, img.size[0]) {
      const char* pixel = row + x*SRDR_SIZEOF_PIXEL_FORMAT(img.format);
      fprintf(dst, "%d %d %d\n", SPLIT3((uint8_t*)pixel));
    }
  }
  fprintf(dst, "# img %s\n", name);

  return RES_OK;
}

static res_T
sdpy_draw
  (struct sdpy* sdpy,
   const struct sdpy_args* args,
   FILE* stream,
   const char* stream_name)
{
  struct srdr_camera* cam = NULL;
  struct s3d_scene_view* scn_view = NULL;
  float pos[3], tgt[3], up[3], proj_ratio, move_sensibility;
  float lower[3], upper[3];
  res_T res = RES_OK;
  ASSERT(sdpy && args && stream);

  /* Build the scene to ray-trace */
  res = s3d_scene_view_create(sdpy->scn, S3D_GET_PRIMITIVE, &scn_view);
  if(res != RES_OK) {
    fprintf(stderr,
      "Couldn't start create a ray-tracing view on the Star3D scene.\n");
    goto error;
  }
  res = s3d_scene_view_get_aabb(scn_view, lower, upper);
  if(res != RES_OK) {
    fprintf(stderr, "Couldn't retrieve the scene view bounding box.\n");
    goto error;
  }
  if(lower[0] >= upper[0] || lower[1] >= upper[1] || lower[2] >= upper[2]) {
    /* Degenerated scene AABB, i.e. empty scene */
    fprintf(stderr, "No displayable object.\n");
    goto exit;
  }

  define_move_sensibility(scn_view, &move_sensibility);

  /* Setup the original point of view */
  proj_ratio = (float)args->vmode[0]/(float)args->vmode[1];
  f3_set(up, args->view_up);
  if(!args->auto_look_at) {
    f3_set(pos, args->view_position);
    f3_set(tgt, args->view_target);
  } else {
    res = auto_look_at(scn_view, args->fov_x, proj_ratio, args->view_up, pos, tgt);
    if(res != RES_OK) {
      fprintf(stderr, "Couldn't compute the automatic point of view.\n");
      goto error;
    }
  }
  res = sdpy_view_setup(sdpy->view, pos, tgt, up, proj_ratio, args->fov_x);
  if(res != RES_OK) goto error;

#ifdef SDPY_USE_SDL
  if(args->interactive) { /* Interactive mode */
    struct sdpy_events events;

    sdpy_input_flush_events(sdpy->input); /* Clean-up the input queue */
    sdpy_events_init(&events);

    for(;;) {

      /* Read user inputs */
      sdpy_events_clear(&events);
      sdpy_input_poll_events(sdpy->input, &events);
      if(events.keys[SDPY_KEY_QUIT] != 0) break;

      /* Update the camera position */
      view_update(sdpy->view, 0.001f, move_sensibility, &events);
      res = sdpy_view_get_srdr_camera(sdpy->view, &cam);
      if(res != RES_OK) goto error;

      if(events.keys[SDPY_KEY_DUMP_IMAGE] != 0) {
        SRDR(framebuffer_setup /* Setup offline framebuffer */
          (sdpy->fbuf, args->vmode, srdr_render_buffer_write, sdpy->rbuf));

        /* Draw and dump the image */
        res = srdr_draw(sdpy->srdr, args->render_type, sdpy->scn, cam, sdpy->fbuf);
        if(res != RES_OK) fprintf(stderr, "Draw error\n");
        res = dump_render_buffer(sdpy->rbuf, stream, stream_name);
        if(res != RES_OK) goto error;

        SRDR(framebuffer_setup /* Restore online framebuffer */
          (sdpy->fbuf, args->vmode, surface_write, sdpy->surface));

        /* Remove the events invoked between 2 polls */
        sdpy_input_flush_events(sdpy->input);

      } else {
        /* Draw into the SDL surface */
        if(SDL_LockSurface(sdpy->surface)) {
          fprintf(stderr, "Couldn't lock SDL surface: %s\n", SDL_GetError());
          return RES_UNKNOWN_ERR;
        }
        res = srdr_draw(sdpy->srdr, args->render_type, sdpy->scn, cam, sdpy->fbuf);
        if(res != RES_OK) fprintf(stderr, "Draw error\n");
        SDL_UnlockSurface(sdpy->surface);
        SDL_Flip(sdpy->surface);
      }
    }
  } else
#endif
  {
    /* Retrieve the camera position */
    res = sdpy_view_get_srdr_camera(sdpy->view, &cam);
    if(res != RES_OK) goto error;
    /* Draw the scene in offline buffer */
    res = srdr_draw(sdpy->srdr, args->render_type, sdpy->scn, cam, sdpy->fbuf);
    if(res != RES_OK) {
      fprintf(stderr, "Error drawing the scene.\n");
      goto error;
    }
    /* Save offline buffer into the destination stream */
    res = dump_render_buffer(sdpy->rbuf, stream, stream_name);
    if(res != RES_OK) goto error;
  }

exit:
  if(scn_view) S3D(scene_view_ref_put(scn_view));
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Main entry point
 ******************************************************************************/
int
main(int argc, char** argv)
{
  struct sdpy_args args = SDPY_ARGS_DEFAULT;
  struct sdpy sdpy = SDPY_NULL;
  struct s3daw* s3daw = NULL;
  struct s3dstl* s3dstl = NULL;
  FILE* out_stream = NULL;
  int err = 0;
  res_T res = RES_OK;
  char exit = 0;

  res = sdpy_args_parse(&args, argc, argv, &exit);
  if(res != RES_OK) goto error;
  if(exit) goto exit;

  res = sdpy_init(&sdpy, &args);
  if(res != RES_OK) goto error;

  if(!args.output_filename) {
    out_stream = stdout;
  } else {
    out_stream = fopen(args.output_filename, "w");
    if(!out_stream) {
      fprintf(stderr, "Couldn't open the destination file '%s'.\n",
        args.output_filename);
      goto error;
    }
  }

  if(args.format == SDPY_GEOM_OBJ
  || args.format == SDPY_GEOM_AUTO
  || !args.nmodels) {
    res = s3daw_create(NULL, NULL, NULL, NULL, sdpy.s3d, 1, &s3daw);
    if(res != RES_OK) {
      fprintf(stderr, "Couldn't create the \"obj\" to Star 3D converter.\n");
      goto error;
    }
  }
  if(args.format == SDPY_GEOM_STL || args.format == SDPY_GEOM_AUTO) {
    res = s3dstl_create(NULL, NULL, NULL, sdpy.s3d, 1, &s3dstl);
    if(res != RES_OK) {
      fprintf(stderr, "Couldn't create the \"stl\" to Star 3D converter.\n");
      goto error;
    }
  }

  if(!args.nmodels) {
    switch(args.format) {
      case SDPY_GEOM_AUTO:
      case SDPY_GEOM_OBJ:
        if(!args.quiet) {
          fprintf(stderr,
"Enter the geometry data with respect to the \"obj\" format.\n"
#ifdef OS_WINDOWS
"Type ^Z (i.e. CTRL+z) and return to stop:\n"
#else
"Type ^D (i.e. CTRL+d) to stop:\n"
#endif
            );
        }
        res = s3daw_load_stream(s3daw, stdin);
        if(res != RES_OK) {
          fprintf(stderr, "Error loading the model from stdin.\n");
          goto error;
        }
        res = register_model_obj(&sdpy, s3daw, "stdin");
        if(res != RES_OK) goto error;
        break;
      case SDPY_GEOM_STL:
        if(!args.quiet) {
          fprintf(stderr,
"Enter the geometry data with respect to the STereo Lithography ASCII format.\n"
#ifdef OS_WINDOWS
"Type ^Z (i.e. CTRL+z) and return to stop:\n"
#else
"Type ^D (i.e. CTRL+d) to stop:\n"
#endif
            );
        }
        res = s3dstl_load_stream(s3dstl, stdin);
        if(res != RES_OK) {
          fprintf(stderr, "Error loading the model from stdin.\n");
          goto error;
        }
        res = register_model_stl(&sdpy, s3dstl, "stdin");
        if(res != RES_OK) goto error;
    }
    res = sdpy_draw(&sdpy, &args, out_stream, "stdin");
    if(res != RES_OK) goto error;
  } else {
    size_t i;
    FOR_EACH(i, 0, args.nmodels) {
      enum sdpy_geom_format format = args.format;
      if(format == SDPY_GEOM_AUTO) {
        const char* ext = strrchr(args.models[i], '.');
        if(!ext) {
          fprintf(stderr,
            "Couldn't find the file format of `%s': extension is missing.\n",
            args.models[i]);
          continue;
        }
        if(!strcmp(ext, ".STL")
        || !strcmp(ext, ".Stl")
        || !strcmp(ext, ".stl")) {
          format = SDPY_GEOM_STL;
        } else if
          (!strcmp(ext, ".OBJ")
        || !strcmp(ext, ".Otl")
        || !strcmp(ext, ".obj")) {
          format = SDPY_GEOM_OBJ;
        }
      }

      switch(format) {
        case SDPY_GEOM_AUTO:
        case SDPY_GEOM_OBJ:
          res = s3daw_load(s3daw, args.models[i]);
          if(res != RES_OK) {
            fprintf(stderr, "Error loading the model '%s'.\n", args.models[i]);
            goto error;
          }
          res = register_model_obj(&sdpy, s3daw, args.models[i]);
          if(res != RES_OK) goto error;
          break;
        case SDPY_GEOM_STL:
          res = s3dstl_load(s3dstl, args.models[i]);
          if(res != RES_OK) {
            fprintf(stderr, "Error loading the model '%s'.\n", args.models[i]);
            goto error;
          }
          res = register_model_stl(&sdpy, s3dstl, args.models[i]);
          if(res != RES_OK) goto error;
          break;
      }
      res = sdpy_draw(&sdpy, &args, out_stream, args.models[i]);
      if(res != RES_OK) goto error;
    }
  }

exit:
  sdpy_release(&sdpy);
  if(s3daw) S3DAW(ref_put(s3daw));
  if(s3dstl) S3DSTL(ref_put(s3dstl));
  if(out_stream && out_stream != stdout) fclose(out_stream);
  if(mem_allocated_size() != 0) {
    fprintf(stderr, "Memory leaks: %lu Bytes\n",
      (unsigned long)mem_allocated_size());
  }
  return err;
error:
  err = 1;
  goto exit;
}

