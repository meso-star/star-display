/* Copyright (C) |Meso|Star> 2015-2017 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#define _POSIX_C_SOURCE 200112L /* getopt & snprintf support */

#include "sdpy_args.h"
#include <rsys/cstr.h>
#ifdef SDPY_USE_SDL
  #include "sdpy_input.h"
#endif

#include <stdio.h>
#include <string.h>

#ifdef COMPILER_CL
  #include <getopt.h>
#else
  #include <unistd.h>
#endif

/*******************************************************************************
 * Helper function
 ******************************************************************************/
static const char*
display_mode_from_srdr_render_type(const enum srdr_render_type rtype)
{
  switch(rtype) {
    case SRDR_RENDER_LEGACY: return "legacy";
    case SRDR_RENDER_NORMAL: return "normal";
    case SRDR_RENDER_FACE_ORIENTATION: return "orientation";
    default: FATAL("Unreachable code\n"); break;
  }
}

#ifdef SDPY_USE_SDL
static const char*
sdpy_input_key_to_string(const SDLKey key)
{
  static char buf[32];
  int i = 0;

  i += snprintf(buf, sizeof(buf), "%s", SDL_GetKeyName(key));
  if((size_t)i >= sizeof(buf)) {
    fprintf(stderr, "Not enough memory. Couldn't build the key name.\n");
    return "<none>";
  }
  return buf;
}

static const char*
sdl_button_to_string(const uint8_t button)
{
  switch(button) {
    case SDL_BUTTON_LEFT: return "Left button";
    case SDL_BUTTON_RIGHT: return "Right button";
    case SDL_BUTTON_MIDDLE: return "Middle button";
    case SDL_BUTTON_WHEELUP: return "Wheel up";
    case SDL_BUTTON_WHEELDOWN: return "Wheel down";
    default: return "unknown";
  }
}
#endif

static void
print_help(const char* binary)
{
  printf(
"Usage: %s [OPTION]... [FILE]...\n"
#ifdef SDPY_USE_SDL
"Feature: interactive SDL display.\n\n"
#endif
"Display an image of each submitted geometry data. FILE must follow the Alias\n"
"Wavefront Obj or the STereo Lithography (STL) file formats. With no FILE, read\n"
"data from standard input. The output image is encoded in the Portable PixMap\n"
"(PPM) file format. The last line of the image is a comment formatted as\n"
"\"# img FILE\".\n\n",
    binary);
  printf(
"By defaut the target and position of the camera are automatically computed in\n"
"order to ensure that the whole model is visible. The \"-p\"  and the \"-t\" options\n"
"override this default comportment by explicitly providing the position and the\n"
"target of the camera, respectively.\n\n");
  printf(
"  -d <legacy|normal|orientation>\n"
"                set the display mode. In \"legacy\" mode, shade the geometry with\n"
"                a white camera light. In \"normal\" mode, display the absolute\n"
"                value of the per pixel geometry normals. In \"orientation\" mode\n"
"                draw the geometry primitives with repect to their orientation;\n"
"                they are rendered in blue or red whether they are front facing\n"
"                or back facing the camera, respectively. Default is \"%s\".\n",
    display_mode_from_srdr_render_type(SDPY_ARGS_DEFAULT.render_type));
  printf(
"  -f FOV        set the horizontal field of view to FOV degree in [30, 120].\n"
"                Default is %.1f.\n",
    MRAD2DEG(SDPY_ARGS_DEFAULT.fov_x));
  printf(
"  -F <obj|stl>  define the format of the submitted FILEs. If not defined, the\n"
"                format is retrieved from the FILE extension or assume to be\n"
"                \"obj\" when data are read from standard input.\n");
  printf(
"  -h            display this help and exit.\n");
#ifdef SDPY_USE_SDL
  printf(
"  -i            enable the interactive mode. Display the loaded geometries in\n"
"                a window with an interactive point of view.\n");
#endif
  printf(
"  -o OUTPUT     write images to OUTPUT. If not defined, write images to stdout.\n");
  printf(
"  -p X:Y:Z      camera position. Default is %.1f:%.1f:%.1f.\n",
    SPLIT3(SDPY_ARGS_DEFAULT.view_position));
  printf(
"  -q            do not print helper message when no FILE is submitted.\n");
  printf(
"  -t X:Y:Z      camera target. Default is %.1f:%.1f:%.1f.\n",
    SPLIT3(SDPY_ARGS_DEFAULT.view_target));
  printf(
"  -u X:Y:Z      camera up vector. Default is %.1f:%.1f:%.1f.\n",
    SPLIT3(SDPY_ARGS_DEFAULT.view_up));
  printf(
"  -v WIDTHxHEIGHT\n"
"                set the image size to WIDTHxHEIGHT. Default is %lux%lu.\n",
    (unsigned long)SDPY_ARGS_DEFAULT.vmode[0],
    (unsigned long)SDPY_ARGS_DEFAULT.vmode[1]);
#ifdef SDPY_USE_SDL
  /* Required by the sdpy_input_key_to_string function */
  if(SDL_Init(SDL_INIT_VIDEO)) {
    fprintf(stderr, "Couldn't initialise SDL: %s\n", SDL_GetError());
    return;
  }
  printf("\n"
"Keyboard actions:\n\n");
  printf(
"  %-13s move camera forward\n", sdpy_input_key_to_string
    (SDPY_INPUT_DEFAULT_KEY_BINDING[SDPY_KEY_MOVE_FORWARD]));
  printf(
"  %-13s move camera backward\n", sdpy_input_key_to_string
    (SDPY_INPUT_DEFAULT_KEY_BINDING[SDPY_KEY_MOVE_BACKWARD]));
  printf(
"  %-13s move camera left\n", sdpy_input_key_to_string
    (SDPY_INPUT_DEFAULT_KEY_BINDING[SDPY_KEY_MOVE_LEFT]));
  printf(
"  %-13s move camera right\n", sdpy_input_key_to_string
    (SDPY_INPUT_DEFAULT_KEY_BINDING[SDPY_KEY_MOVE_RIGHT]));
  printf(
"  %-13s move camera up\n", sdpy_input_key_to_string
    (SDPY_INPUT_DEFAULT_KEY_BINDING[SDPY_KEY_MOVE_UP]));
  printf(
"  %-13s move camera down\n", sdpy_input_key_to_string
    (SDPY_INPUT_DEFAULT_KEY_BINDING[SDPY_KEY_MOVE_DOWN]));
  printf(
"  %-13s look camera up\n", sdpy_input_key_to_string
    (SDPY_INPUT_DEFAULT_KEY_BINDING[SDPY_KEY_LOOK_UP]));
  printf(
"  %-13s look camera down\n", sdpy_input_key_to_string
    (SDPY_INPUT_DEFAULT_KEY_BINDING[SDPY_KEY_LOOK_DOWN]));
  printf(
"  %-13s look camera left\n", sdpy_input_key_to_string
    (SDPY_INPUT_DEFAULT_KEY_BINDING[SDPY_KEY_LOOK_LEFT]));
  printf(
"  %-13s look camera right\n", sdpy_input_key_to_string
    (SDPY_INPUT_DEFAULT_KEY_BINDING[SDPY_KEY_LOOK_RIGHT]));
  printf(
"  %-13s dump image\n", sdpy_input_key_to_string
    (SDPY_INPUT_DEFAULT_KEY_BINDING[SDPY_KEY_DUMP_IMAGE]));
  printf(
"  %-13s quit %s \n", sdpy_input_key_to_string
    (SDPY_INPUT_DEFAULT_KEY_BINDING[SDPY_KEY_QUIT]), binary);

  printf("\n"
"Mouse actions:\n\n");
  printf(
"  %-13s free look while dragging\n", sdl_button_to_string
    (SDPY_INPUT_DEFAULT_BUTTON_BINDING[SDPY_BUTTON_MOTION_LOOK]));
  printf(
"  %-13s move camera up, down, left and right while dragging\n",
    sdl_button_to_string
      (SDPY_INPUT_DEFAULT_BUTTON_BINDING[SDPY_BUTTON_MOTION_MOVE]));
  printf(
"  %-13s move camera forward\n",
    sdl_button_to_string
      (SDPY_INPUT_DEFAULT_BUTTON_BINDING[SDPY_BUTTON_MOVE_FORWARD]));
  printf(
"  %-13s move camera backward\n",
    sdl_button_to_string
      (SDPY_INPUT_DEFAULT_BUTTON_BINDING[SDPY_BUTTON_MOVE_BACKWARD]));
  SDL_Quit();
#endif
}

static res_T
parse_video_mode(const char* str, size_t vmode[3])
{
  char buf[128];
  char* tk;
  unsigned long l;
  res_T res = RES_OK;
  ASSERT(str && vmode);

  if(strlen(str) >= sizeof(buf) - 1/*NULL char*/)
    return RES_MEM_ERR;
  strncpy(buf, str, sizeof(buf));

  tk = strtok(buf, "x");
  res = cstr_to_ulong(tk, &l);
  if(res != RES_OK) return res;
  vmode[0] = (size_t)l;

  tk = strtok(NULL, "");
  res = cstr_to_ulong(tk, &l);
  if(res != RES_OK) return res;
  vmode[1] = (size_t)l;

  return res;
}

static res_T
parse_fov(const char* str, float* fov)
{
  float tmp;
  res_T res;
  ASSERT(str && fov);
  res = cstr_to_float(str, &tmp);
  if(res != RES_OK) return res;
  if(tmp < 30.f || tmp > 120.f) return RES_BAD_ARG;
  *fov = (float)MDEG2RAD(tmp);
  return RES_OK;
}

static res_T
parse_geom_format(const char* str, enum sdpy_geom_format* fmt)
{
  res_T res = RES_OK;
  ASSERT(str && fmt);
  if(!strcmp(str, "obj") || !strcmp(str, "Obj") || !strcmp(str, "OBJ")) {
    *fmt = SDPY_GEOM_OBJ;
  } else if(!strcmp(str, "stl") || !strcmp(str, "Stl") || !strcmp(str, "STL")) {
    *fmt = SDPY_GEOM_STL;
  } else {
    res = RES_BAD_ARG;
  }
  return res;
}

static res_T
parse_display_mode(const char* str, enum srdr_render_type* rtype)
{
  res_T res = RES_OK;
  ASSERT(str && rtype);
  if(!strcmp(str, "legacy")) {
    *rtype = SRDR_RENDER_LEGACY;
  } else if(!strcmp(str, "normal")) {
    *rtype = SRDR_RENDER_NORMAL;
  } else if(!strcmp(str, "orientation")) {
    *rtype = SRDR_RENDER_FACE_ORIENTATION;
  } else {
    res = RES_BAD_ARG;
  }
  return res;
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
sdpy_args_parse
  (struct sdpy_args* args, const int argc, char** argv, char* exit)
{
  size_t len;
  int opt;
  const char* options = "d:f:F:ho:p:qt:u:v:"
#ifdef SDPY_USE_SDL
    "i"
#endif
    ;
  res_T res = RES_OK;
  ASSERT(argc && argv && args && exit);

  *args = SDPY_ARGS_DEFAULT;
  while((opt = getopt(argc, argv, options)) != -1) {
    switch(opt) {
      case 'd': res = parse_display_mode(optarg, &args->render_type); break;
      case 'f': res = parse_fov(optarg, &args->fov_x); break;
      case 'F': res = parse_geom_format(optarg, &args->format); break;
      case 'h': print_help(argv[0]); *exit = 1; return RES_OK;
#ifdef SDPY_USE_SDL
      case 'i': args->interactive = 1; break;
#endif
      case 'o': args->output_filename = optarg; break;
      case 'p':
        res = cstr_to_list_float(optarg, ':', args->view_position, &len, 3);
        if(res == RES_OK && len != 3) res = RES_BAD_ARG;
        args->auto_look_at = 0; /* Disable auto look at */
        break;
      case 'q': args->quiet = 1; break;
      case 't':
        res = cstr_to_list_float(optarg, ':', args->view_target, &len, 3);
        if(res == RES_OK && len != 3) res = RES_BAD_ARG;
        args->auto_look_at = 0; /* Disable auto look at */
        break;
      case 'u':
        res = cstr_to_list_float(optarg, ':', args->view_up, &len, 3);
        if(res == RES_OK && len != 3) res = RES_BAD_ARG;
        break;
      case 'v': res = parse_video_mode(optarg, args->vmode); break;
      default: res = RES_BAD_ARG; break;
    }
    if(res != RES_OK) {
      if(optarg) {
        fprintf(stderr, "%s: invalid option arguments '%s' -- '%c'\n",
          argv[0], optarg, opt);
      }
      goto error;
    }
  }
  args->models = argv + optind;
  args->nmodels = (size_t)(argc - optind);
exit:
  return res;
error:
  *args = SDPY_ARGS_DEFAULT;
  goto exit;
}

