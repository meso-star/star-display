/* Copyright (C) |Meso|Star> 2015-2017 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#ifndef SDPY_ARGS_H
#define SDPY_ARGS_H

#include <rsys/math.h>
#include <rsys/rsys.h>

#include <star/srdr.h>

enum sdpy_geom_format {
  SDPY_GEOM_OBJ,
  SDPY_GEOM_STL,
  SDPY_GEOM_AUTO
};

struct sdpy_args {
  float view_position[3];
  float view_target[3];
  float view_up[3];
  size_t vmode[2]; /* width, height, bpp */
  float fov_x; /* Horizontal field of view */
  enum sdpy_geom_format format;
  enum srdr_render_type render_type;
  const char* output_filename;
  char** models;
  size_t nmodels;
  char quiet; /* Do not print helper message when reading data from stdin */
  char auto_look_at;
#ifdef SDPY_USE_SDL
  char interactive;
#endif
};

static const struct sdpy_args SDPY_ARGS_DEFAULT = {
  {0.f, 0.f, 1.f}, /* View position */
  {0.f, 0.f, 0.f}, /* View target */
  {0.f, 1.f, 0.f}, /* View up */
  {640, 480}, /* Width x Height */
  1.2217304764f, /* FOV ~ 70 degree */
  SDPY_GEOM_AUTO, /* Geometry format */
  SRDR_RENDER_LEGACY, /* Render type */
  NULL, /* Output filename */
  NULL, /* List of input models */
  0, /* # input models */
  0, /* Quiet */
  1, /* Auto look at */
#ifdef SDPY_USE_SDL
  0, /* Interactive */
#endif
};

extern LOCAL_SYM res_T
sdpy_args_parse
  (struct sdpy_args* args,
   const int argc,
   char** argv,
   char* exit);

#endif /* SDPY_ARGS_H */

