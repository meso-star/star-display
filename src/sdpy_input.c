/* Copyright (C) |Meso|Star> 2015-2017 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include "sdpy_input.h"

#include <rsys/hash_table.h>
#include <rsys/mem_allocator.h>
#include <rsys/ref_count.h>

#include <limits.h>

/* Declare the key_binding hash table */
#define HTABLE_NAME key_binding
#define HTABLE_KEY SDLKey
#define HTABLE_DATA enum sdpy_input_key
#include <rsys/hash_table.h>

/* Declare the button_binding hash table */
#define HTABLE_NAME button_binding
#define HTABLE_KEY uint8_t
#define HTABLE_DATA enum sdpy_input_button
#include <rsys/hash_table.h>

struct sdpy_input {
  struct htable_key_binding key_binding;
  struct htable_button_binding button_binding;
  ref_T ref;
};

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static FINLINE void
on_key_event
  (struct sdpy_input* input, struct sdpy_events* events, const SDL_Event* evt)
{
  enum sdpy_input_key* ikey;
  ASSERT(input && events && evt);
  ASSERT(evt->type == SDL_KEYDOWN || evt->type == SDL_KEYUP);

  ikey = htable_key_binding_find(&input->key_binding, &evt->key.keysym.sym);
  if(!ikey) return; /* Key is not bound */

  ASSERT(*ikey < SDPY_KEYS_COUNT__);

  if(*ikey == SDPY_KEY_DUMP_IMAGE) {
    if(evt->type == SDL_KEYUP) {
      events->is_dumping__ = 0;
    } else if(!events->is_dumping__) {
      events->is_dumping__ = 1;
      events->keys[SDPY_KEY_DUMP_IMAGE] = 1;
    }
  } else if(*ikey == SDPY_KEY_QUIT) {
    if(evt->type == SDL_KEYUP) {
      events->is_quiting__ = 0;
    } else if(!events->is_quiting__) {
      events->is_quiting__ = 1;
      events->keys[SDPY_KEY_QUIT] = 1;
    }
  } else{
    events->keys[*ikey] += events->keys[*ikey] < INT_MAX;
  }
}

static FINLINE void
on_mouse_motion_event
  (struct sdpy_input* input, struct sdpy_events* events, const SDL_Event* evt)
{
  ASSERT(input && events && evt && evt->type == SDL_MOUSEMOTION);
  events->mouse_dx += evt->motion.xrel;
  events->mouse_dy += evt->motion.yrel;
  (void)input;
}

static FINLINE void
on_mouse_button_event
  (struct sdpy_input* input, struct sdpy_events* events, const SDL_Event* evt)
{
  enum sdpy_input_button* ibutton;
  ASSERT(input && events && evt);
  ASSERT(evt->type == SDL_MOUSEBUTTONDOWN || evt->type == SDL_MOUSEBUTTONUP);

  ibutton = htable_button_binding_find
    (&input->button_binding, &evt->button.button);
  if(!ibutton) return; /* Button is not bound */

  switch(*ibutton) {
    case SDPY_BUTTON_MOTION_LOOK:
    case SDPY_BUTTON_MOTION_MOVE:
      if(evt->button.state == SDL_PRESSED) {
        events->buttons |= SDPY_BUTTON_FLAG(*ibutton);
      } else {
        events->buttons = events->buttons & ~SDPY_BUTTON_FLAG(*ibutton);
      }
      break;
    case SDPY_BUTTON_MOVE_FORWARD:
      events->keys[SDPY_KEY_MOVE_FORWARD] +=
        events->keys[SDPY_KEY_MOVE_FORWARD] < INT_MAX;
      break;
    case SDPY_BUTTON_MOVE_BACKWARD:
      events->keys[SDPY_KEY_MOVE_BACKWARD] +=
        events->keys[SDPY_KEY_MOVE_BACKWARD] < INT_MAX;
      break;
    default: FATAL("Unreachable code\n"); break;
  }
}

static void
input_release(ref_T* ref)
{
  struct sdpy_input* input;
  ASSERT(ref);
  input = CONTAINER_OF(ref, struct sdpy_input, ref);
  htable_key_binding_release(&input->key_binding);
  htable_button_binding_release(&input->button_binding);
  mem_rm(input);
}

/*******************************************************************************
 * Local function
 ******************************************************************************/
res_T
sdpy_input_create
  (const SDLKey keys[SDPY_KEYS_COUNT__],
   const uint8_t buttons[SDPY_BUTTONS_COUNT__],
   struct sdpy_input** out_input)
{
  struct sdpy_input* input = NULL;
  enum sdpy_input_key ikey;
  enum sdpy_input_button ibutton;
  res_T res = RES_OK;
  ASSERT(keys && out_input);

  input = mem_calloc(1, sizeof(struct sdpy_input));
  if(!input) {
    fprintf(stderr, "Couldn't allocate the input data structure.\n");
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&input->ref);

  /* Init the key binding */
  htable_key_binding_init(&mem_default_allocator, &input->key_binding);
  FOR_EACH(ikey, 0, SDPY_KEYS_COUNT__) {
    res = htable_key_binding_set(&input->key_binding, keys + ikey, &ikey);
    if(res != RES_OK) goto error;
  }

  /* Init the mouse binding */
  htable_button_binding_init(&mem_default_allocator, &input->button_binding);
  FOR_EACH(ibutton, 0, SDPY_BUTTONS_COUNT__) {
    res = htable_button_binding_set
      (&input->button_binding, buttons + ibutton, &ibutton);
    if(res != RES_OK) goto error;
  }

exit:
  *out_input = input;
  return res;
error:
  if(input) {
    sdpy_input_ref_put(input);
    input = NULL;
  }
  goto exit;
}

void
sdpy_input_ref_get(struct sdpy_input* input)
{
  ASSERT(input);
  ref_get(&input->ref);
}

void
sdpy_input_ref_put(struct sdpy_input* input)
{
  ASSERT(input);
  ref_put(&input->ref, input_release);
}

void
sdpy_input_poll_events(struct sdpy_input* input, struct sdpy_events* events)
{
  SDL_Event evt;
  ASSERT(input && events);

  while(SDL_PollEvent(&evt)) {
    switch(evt.type) {
      case SDL_KEYUP:
      case SDL_KEYDOWN:
        on_key_event(input, events, &evt);
        break;
      case SDL_MOUSEMOTION:
        on_mouse_motion_event(input, events, &evt);
        break;
      case SDL_MOUSEBUTTONUP:
      case SDL_MOUSEBUTTONDOWN:
        on_mouse_button_event(input, events, &evt);
        break;
      default: /* Silently discard untracked events */ break;
    }
  }
}

void
sdpy_input_flush_events(struct sdpy_input* input)
{
  SDL_Event evt;
  ASSERT(input);
  (void)input;
  while(SDL_PollEvent(&evt));
}

