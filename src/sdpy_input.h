/* Copyright (C) |Meso|Star> 2015-2017 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#ifndef SDPY_INPUT_H
#define SDPY_INPUT_H

#include <rsys/rsys.h>
#include <SDL.h>

enum sdpy_input_key { /* List of input keys */
  SDPY_KEY_MOVE_LEFT,
  SDPY_KEY_MOVE_RIGHT,
  SDPY_KEY_MOVE_UP,
  SDPY_KEY_MOVE_DOWN,
  SDPY_KEY_MOVE_FORWARD,
  SDPY_KEY_MOVE_BACKWARD,
  SDPY_KEY_LOOK_UP,
  SDPY_KEY_LOOK_DOWN,
  SDPY_KEY_LOOK_LEFT,
  SDPY_KEY_LOOK_RIGHT,
  SDPY_KEY_QUIT,
  SDPY_KEY_DUMP_IMAGE,
  SDPY_KEYS_COUNT__
};

enum sdpy_input_button { /* List of input buttons */
  SDPY_BUTTON_MOTION_LOOK,
  SDPY_BUTTON_MOTION_MOVE,
  SDPY_BUTTON_MOVE_FORWARD,
  SDPY_BUTTON_MOVE_BACKWARD,
  SDPY_BUTTONS_COUNT__
};

/* Helper macro used as syntactic sugar */
#define SDPY_BUTTON_FLAG(Button) BIT(Button)

struct sdpy_input_key_desc {
  SDLKey key; /* Keyboard key */
};

static const SDLKey
SDPY_INPUT_DEFAULT_KEY_BINDING[SDPY_KEYS_COUNT__] = { /* Default key binding */
  SDLK_q, /* Move left */
  SDLK_d, /* Move right */
  SDLK_SPACE, /* Move up */
  SDLK_c, /* Move down */
  SDLK_z, /* Move forward */
  SDLK_s, /* Move backward */
  SDLK_k, /* Look up */
  SDLK_j, /* Look down */
  SDLK_h, /* Look left */
  SDLK_l, /* Look right */
  SDLK_ESCAPE, /* Quit */
  SDLK_w, /* Write image */
};

static const uint8_t
SDPY_INPUT_DEFAULT_BUTTON_BINDING[SDPY_BUTTONS_COUNT__] = { /* Default binding */
  SDL_BUTTON_RIGHT, /* Motion Look */
  SDL_BUTTON_LEFT, /* Motion Move */
  SDL_BUTTON_WHEELUP, /* Move forward */
  SDL_BUTTON_WHEELDOWN /* Move backward */
};

/*******************************************************************************
 * Events data structure
 ******************************************************************************/
struct sdpy_events {
  int keys[SDPY_KEYS_COUNT__];
  int buttons; /* Combination of SDBY_BUTTON_FLAG(<enum sdpy_input_button>) */
  int mouse_dx;
  int mouse_dy;

  int is_dumping__; /* Register a dump image invocation */
  int is_quiting__; /* Register a quit invocation */
};

static INLINE void
sdpy_events_init(struct sdpy_events* events)
{
  ASSERT(events);
  memset(events, 0, sizeof(struct sdpy_events));
}

static INLINE void
sdpy_events_clear(struct sdpy_events* events)
{
  int pressed_buttons;
  int is_dumping;
  int is_quiting;
  ASSERT(events);
  /* Save some event states */
  pressed_buttons = events->buttons;
  is_dumping = events->is_dumping__;
  is_quiting = events->is_quiting__;
  /* Clean up the events */
  memset(events, 0, sizeof(struct sdpy_events));
  /* Restore the saved events */
  events->buttons = pressed_buttons;
  events->is_dumping__ = is_dumping;
  events->is_quiting__ = is_quiting;
}

/*******************************************************************************
 * Input manager
 ******************************************************************************/
struct sdpy_input;

extern LOCAL_SYM res_T
sdpy_input_create
  (const SDLKey binding[SDPY_KEYS_COUNT__],
   const uint8_t button_binding[SDPY_BUTTONS_COUNT__],
   struct sdpy_input** input);

extern LOCAL_SYM void
sdpy_input_ref_get
  (struct sdpy_input* input);

extern LOCAL_SYM void
sdpy_input_ref_put
  (struct sdpy_input* input);

/* Poll for all currently pending events */
extern LOCAL_SYM void
sdpy_input_poll_events
  (struct sdpy_input* input,
   struct sdpy_events* events);

extern LOCAL_SYM void
sdpy_input_flush_events
  (struct sdpy_input* input);

#endif /* SDPY_INPUT_H */

