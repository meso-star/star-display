/* Copyright (C) |Meso|Star> 2015-2017 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include "sdpy_view.h"

#include <rsys/float3.h>
#include <rsys/float33.h>
#include <rsys/mem_allocator.h>
#include <rsys/ref_count.h>

#include <star/srdr.h>

struct sdpy_view {
  struct srdr_camera* srdr_cam;

  float axis_x[3], axis_y[3], axis_z[3]; /* Orthonormal basis */
  float zoom;
  float position[3];
  float up[3];
  char is_srdr_cam_outdated;

  ref_T ref;
};

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
view_release(ref_T* ref)
{
  struct sdpy_view* view;
  ASSERT(ref);
  view = CONTAINER_OF(ref, struct sdpy_view, ref);
  if(view->srdr_cam) SRDR(camera_ref_put(view->srdr_cam));
  mem_rm(view);
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
sdpy_view_create(struct srdr_device* srdr, struct sdpy_view** out_view)
{
  struct sdpy_view* view = NULL;
  res_T res = RES_OK;
  ASSERT(srdr && out_view);

  view = mem_calloc(1, sizeof(struct sdpy_view));
  if(!view) {
    fprintf(stderr,
      "Not enough memory: couldn't allocate the interactive point of view.\n");
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&view->ref);
  res = srdr_camera_create(srdr, &view->srdr_cam);
  if(res != RES_OK) {
    fprintf(stderr, "Couldn't create the Star-Renderer camera.\n");
    goto error;
  }

exit:
  *out_view = view;
  return res;
error:
  if(view) {
    sdpy_view_ref_put(view);
    view = NULL;
  }
  goto exit;
}

void
sdpy_view_ref_get(struct sdpy_view* view)
{
  ref_get(&view->ref);
}

void
sdpy_view_ref_put(struct sdpy_view* view)
{
  ref_put(&view->ref, view_release);
}

res_T
sdpy_view_setup
  (struct sdpy_view* view,
   const float pos[3],
   const float target[3],
   const float up[3],
   const float proj_ratio,
   const float fov_x)
{
  struct srdr_camera_desc cam_desc;
  res_T res = RES_OK;
  ASSERT(view && pos && target && up);

  f3_set(cam_desc.position, pos);
  f3_set(cam_desc.target, target);
  f3_set(cam_desc.up, up);
  cam_desc.proj_ratio = proj_ratio;
  cam_desc.fov_x = fov_x;
  res = srdr_camera_setup(view->srdr_cam, &cam_desc);
  if(res != RES_OK) {
    fprintf(stderr,
      "Couldn't setup camera of the Star-Renderer:\n"
      "  position: %f %f %f\n"
      "  target: %f %f %f\n"
      "  up: %f %f %f\n"
      "  projection ratio: %f\n"
      "  horizontal field of view: %f\n",
      SPLIT3(pos), SPLIT3(target), SPLIT3(up), proj_ratio, fov_x);
    goto error;
  }
  res = sdpy_view_set_look_at(view, pos, target, up);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  goto exit;
}

res_T
sdpy_view_set_look_at
  (struct sdpy_view* view,
   const float pos[3],
   const float target[3],
   const float up[3])
{
  float axis_x[3], axis_y[3], axis_z[3];
  float zoom;
  res_T res = RES_OK;
  ASSERT(view && pos && target && up);

  f3_sub(axis_z, target, pos);
  zoom = f3_normalize(axis_z, axis_z);
  if(zoom <= 0.f) {
    res = RES_BAD_ARG;
    goto error;
  }
  f3_cross(axis_x, axis_z, up);
  if(f3_normalize(axis_x, axis_x) <= 0.f) {
    res = RES_BAD_ARG;
    goto error;
  }
  f3_cross(axis_y, axis_x, axis_z);
  if(f3_normalize(axis_y, axis_y) <= 0.f) {
    res = RES_BAD_ARG;
    goto error;
  }

  view->zoom = zoom;
  f3_set(view->position, pos);
  f3_set(view->up, up);
  f3_set(view->axis_x, axis_x);
  f3_set(view->axis_y, axis_y);
  f3_set(view->axis_z, axis_z);
  view->is_srdr_cam_outdated = 1;

exit:
  return res;
error:
  fprintf(stderr,
    "Couldn't setup the point of view:\n"
    "  position: %.2f %.2f %.2f\n"
    "  target: %.2f %.2f %.2f\n"
    "  up: %.2f %.2f %.2f\n",
    SPLIT3(pos), SPLIT3(target), SPLIT3(up));
  goto exit;
}

res_T
sdpy_view_get_look_at
  (const struct sdpy_view* view,
   float pos[3],
   float target[3],
   float up[3])
{
  ASSERT(view && pos && target && up);
  f3_set(pos, view->position);
  f3_add(target, pos, f3_mulf(target, view->axis_z, view->zoom));
  f3_set(up, view->up);
  return RES_OK;
}

res_T
sdpy_view_set_fov(struct sdpy_view* view, const float fov_x)
{
  res_T res = RES_OK;
  ASSERT(view);
  res = srdr_camera_set_fov(view->srdr_cam, fov_x);
  if(res != RES_OK) {
    fprintf(stderr,
      "Could not set the horizontal field of view  to `%.2f' degree\n",
      MRAD2DEG(fov_x));
  }
  return res;
}

res_T
sdpy_view_get_fov(struct sdpy_view* view, float* fov)
{
  res_T res = RES_OK;
  ASSERT(view && fov);
  res = srdr_camera_get_fov(view->srdr_cam, fov);
  if(res != RES_OK) {
    fprintf(stderr, "Could not retrieve the horizontal field of view\n");
    *fov = FLT_MAX;
  }
  return res;
}

res_T
sdpy_view_translate(struct sdpy_view* view, const float trans[3])
{
  float tmp0[3], tmp1[3];
  ASSERT(view && trans);

  if(f3_eq_eps(trans, f3_splat(tmp0, 0), 1.e-6f))
    return RES_OK; /* Ignore ~0 translations */

  f3_mulf(tmp0, view->axis_x, trans[0]);
  f3_add(tmp0, tmp0, f3_mulf(tmp1, view->up, trans[1]));
  f3_add(tmp0, tmp0, f3_mulf(tmp1, view->axis_z, trans[2]));
  f3_add(view->position, view->position, tmp0);
  view->is_srdr_cam_outdated = 1;
  return RES_OK;
}

res_T
sdpy_view_rotate(struct sdpy_view* view, const float rot[3])
{
  float rot_X[9], rot_Up[9], mat[9];
  ASSERT(view && rot);

  if(f3_eq_eps(rot, f3_splat(rot_X, 0), 1.e-6f))
    return RES_OK; /* Ignore ~0 translations */

  f33_rotation_pitch(rot_X, rot[0]);
  f33_rotation_axis_angle(rot_Up, view->up, rot[1]);

  /* Current rotation matrix */
  f33_set_row(mat, view->axis_x, 0);
  f33_set_row(mat, view->axis_y, 1);
  f33_set_row(mat, view->axis_z, 2);

  f33_mulf33(mat, rot_X, mat); /* Rotate around the local X axis */
  f33_mulf33(mat, mat, rot_Up); /* Rotate around the global Up axis */

  /* Update the view basis */
  f33_row(view->axis_x, mat, 0);
  f33_row(view->axis_y, mat, 1);
  f33_row(view->axis_z, mat, 2);

  /* Revert the up vector if the `head is down' */
  if(f3_dot(view->axis_y, view->up) < 0.f)
    f3_minus(view->up, view->up);

  view->is_srdr_cam_outdated = 1;
  return RES_OK;
}


res_T
sdpy_view_get_srdr_camera
  (const struct sdpy_view* view,
   struct srdr_camera** srdr_cam)
{
  float pos[3], tgt[3], up[3];
  res_T res = RES_OK;
  ASSERT(view && srdr_cam);

  if(!view->is_srdr_cam_outdated) {
    *srdr_cam = view->srdr_cam;
    return RES_OK;
  }

  sdpy_view_get_look_at(view, pos, tgt, up);
  res = srdr_camera_look_at(view->srdr_cam, pos,  tgt, up);
  if(res != RES_OK) {
    fprintf(stderr,
      "Could not setup the point of view of the camera:\n"
      "  position: %f %f %f\n"
      "  target: %f %f %f\n"
      "  up: %f %f %f\n",
    SPLIT3(pos), SPLIT3(tgt), SPLIT3(up));
    *srdr_cam = NULL;
    return res;
  }
  *srdr_cam = view->srdr_cam;
  return RES_OK;
}

