/* Copyright (C) |Meso|Star> 2015-2017 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#ifndef SDPY_VIEW_H
#define SDPY_VIEW_H

#include <rsys/rsys.h>

struct mem_allocator;
struct sdpy_view;
struct srdr_camera;
struct srdr_device;

extern LOCAL_SYM res_T
sdpy_view_create
  (struct srdr_device* dev,
   struct sdpy_view** view);

extern LOCAL_SYM void
sdpy_view_ref_get
  (struct sdpy_view* view);

extern LOCAL_SYM void
sdpy_view_ref_put
  (struct sdpy_view* view);

extern LOCAL_SYM res_T
sdpy_view_setup
  (struct sdpy_view* view,
   const float pos[3],
   const float target[3],
   const float up[3],
   const float proj_ratio, /* width / height */
   const float fov_x); /* In radian */

extern LOCAL_SYM res_T
sdpy_view_set_look_at
  (struct sdpy_view* view,
   const float pos[3],
   const float target[3],
   const float up[3]);

extern LOCAL_SYM res_T
sdpy_view_get_look_at
  (const struct sdpy_view* view,
   float pos[3],
   float target[3],
   float up[3]);

/* Set the horizontal field of view */
extern LOCAL_SYM res_T
sdpy_view_set_fov
  (struct sdpy_view* view,
   const float fov_x); /* In radians */

extern LOCAL_SYM res_T
sdpy_view_get_fov
  (struct sdpy_view* view,
   float* fov_x);

extern LOCAL_SYM res_T
sdpy_view_translate
  (struct sdpy_view* view,
   const float trans[3]);

extern LOCAL_SYM res_T
sdpy_view_rotate
  (struct sdpy_view* view,
   const float rot[3]);

extern LOCAL_SYM res_T
sdpy_view_get_srdr_camera
  (const struct sdpy_view* view,
   struct srdr_camera** srdr_cam);

#endif /* SDPY_VIEW_H */

